# ドラクエウォークこころ確定/高確率マップ取得調査
---
## 調査結果
取得した Protobuf からこころ情報を取得するのは困難

## 調査手法

1. こころチャレンジ可能なレベルまでレベルをあげる
2. こころ確定スポットを検索
3. こころ確定スポットを含む地図タイルを Protobuf として取得
4. Protobuf を `pbtext2json.rb` で Json 化

## クエリストリングについて

### 座標

- 例. 'https://vectortile.googleapis.com/v1alpha/tiles/@116396,51609,17z?key=AIzaSyDTR4-trQ9IHwDyax6eVvzJ3qSoOrYQKbY&enableModeledVolumes=0&languageCode=ja&regionCode=JP&alt=proto&enableFeatureNames=true&enablePoliticalFeatures=0&enableUnclippedBuildings=true&enableDetailedHighwayTypes=true&enablePrivateRoads=0'
- この場合， `@116396,51609,17z?` 部分が Google Maps Gaming Solution の提供する vector_tile の座標に相当する
- `116396` が東西成分，`51609` が南北成分
  - この値を変更することで，取得タイル情報は偽装可能と思われる
- `17z` が何を示すかは不明
- Google 画像検索で `@` 以下の数値を調べると，周辺地図の画像が検索上位にヒット
- 通常の GoogleMaps の座標表記とは異なる

### vector_tile

- https://developers.google.com/maps/documentation/gaming/reference/unity/class/google/maps/maps-service
- ドキュメントによれば，クライアントサイドからはこのサービスが提供する API は使えない

### v1alpha

- https://godoc.org/github.com/coreos/rkt/api/v1alpha
- protocol buffer 生成用パッケージ
