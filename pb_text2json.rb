# frozen_string_literal: true

result_json = ''
brackets_keys_stack = []

result_json += '{'
brackets_keys_stack.push(bracket: 'hash', key: nil)

pbtxt = File.read('tile41.txt')

pbtxt.each_line do |line|
  line = line.chomp
  line += ',' unless line.match(/{$/)

  if line.match(/^\s*\d+:\s/)
    if brackets_keys_stack.last[:bracket] == 'array'
      result_json += '],'
      brackets_keys_stack.pop
    end
    if line.match(/^\s*(\d+):\s\"(.*)\"/)
      new_line = line.gsub(/^\s*\d+:\s/, %("#{Regexp.last_match(1)}":))
      new_line_array = new_line.split(%(":"))
      text_value = eval('"' + new_line_array[1].gsub(/,$/, ''))
      text_value.gsub!("\n", '\\n') if text_value.include?("\n")
      text_value.gsub!('"', '\"') if text_value.include?('"')
      new_line = new_line_array[0] + '":"' + text_value + '",'
    elsif line.match(/^\s*(\d+):\s/)
      new_line = line.gsub(/^\s*\d+:\s/, %("#{Regexp.last_match(1)}":")).gsub(/,$/, %(",))
    end
    result_json += new_line
  elsif line.match(/^\s*(\d+)\s{/)
    if brackets_keys_stack.last[:bracket] == 'array'
      if Regexp.last_match(1) == brackets_keys_stack.last[:key]
        brackets_keys_stack.push(bracket: 'hash', key: nil)
        new_line = line.gsub(/^\s*\d+\s{/, '{')
        result_json += new_line
      elsif Regexp.last_match(1) != brackets_keys_stack.last[:key]
        result_json += '],'
        brackets_keys_stack.pop
        brackets_keys_stack.push(bracket: 'array', key: Regexp.last_match(1))
        brackets_keys_stack.push(bracket: 'hash', key: nil)
        new_line = line.gsub(/^\s*\d+\s{/, %("#{Regexp.last_match(1)}":[{))
        result_json += new_line
      end
    else
      brackets_keys_stack.push(bracket: 'array', key: Regexp.last_match(1))
      brackets_keys_stack.push(bracket: 'hash', key: nil)
      new_line = line.gsub(/^\s*\d+\s{/, %("#{Regexp.last_match(1)}":[{))
      result_json += new_line
    end
  elsif line.match(/^\s*}/)
    if brackets_keys_stack.last[:bracket] == 'array'
      new_line = line.gsub(/^\s*/, ']')
      brackets_keys_stack.pop
    else
      new_line = line.gsub(/^\s*/, '')
    end
    result_json += new_line
    brackets_keys_stack.pop
  else
    new_line = line.gsub(/^\s*/, '')
    result_json += new_line
  end
end

if brackets_keys_stack.last[:bracket] == 'array'
  result_json += ']}'
  brackets_keys_stack.pop
else
  result_json += '}'
end
brackets_keys_stack.pop

result_json = result_json.gsub(',}', '}').gsub(',]', ']')
puts result_json
